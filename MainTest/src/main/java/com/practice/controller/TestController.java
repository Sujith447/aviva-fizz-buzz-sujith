package com.practice.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	/**
	 * Test case.
	 * 
	 * @param pojo
	 *            the pojo
	 * @throws IOException
	 */
	@Autowired
	DayChecker dayChecker;

	@GetMapping(value = "/abc")
	public void testCase(RequestPojo pojo, HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter print = res.getWriter();

		String checkIfWednesday = dayChecker.dayCheck();
		for (int i = 1; i <= pojo.getInputNumber(); i++) {
			int flag = 1;
			if (i % 3 == 0 && i % 5 == 0) {
				if (checkForWednesday(checkIfWednesday)) {
					print.println("wizz wuzz");
					flag = 0;
				}
				print.println("fizz buzz");
				flag = 0;
			} else {
				if (!(i % 3 == 0 || i % 5 == 0)) {
					print.println(i);
				}
			}
			if (i % 3 == 0 && flag != 0) {
				if (checkForWednesday(checkIfWednesday)) {
					print.println("wizz");
				}
				print.println("fizz");
			}

			if (i % 5 == 0 && flag != 0) {
				if (checkForWednesday(checkIfWednesday)) {
					print.println("wuzz");
				}
				print.println("buzz");
			}

		}

	}

	/**
	 * Check for wednesday. Checking whether it is wednesday or not
	 * 
	 * @param checkIfWednesday
	 *            the check if wednesday
	 * @return true, if successful
	 */
	private static boolean checkForWednesday(String checkIfWednesday) {
		return "Wednesday".equals(checkIfWednesday);
	}

	/**
	 * Request binding.
	 *
	 * @param number
	 *            the number
	 * @param next
	 *            the next
	 * @param previous
	 *            the previous
	 */
	@ModelAttribute
	public void requestBinding(@RequestParam("number") int number, @RequestParam("Next") int next,
			@RequestParam("Previous") int previous, RequestPojo pojo) {
		if (number > 1 && number < 1000) {
			System.out.println("The given number is a valid number");
			pojo.setInputNumber(number);
			pojo.setNext(next);
			pojo.setPrevious(previous);
		} else {
			throw new CustomException("Invalid number");
		}
	}

}
