package com.practice.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class DayChecker {

	/**
	 * Day check.
	 *
	 * @return the string
	 */
	public String dayCheck() {

		Date now = new Date();
		SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE"); 
		return simpleDateformat.format(now);
	}

}
