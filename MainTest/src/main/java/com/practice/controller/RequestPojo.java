package com.practice.controller;


public class RequestPojo {
	
	public int inputNumber;
	
	public int next;
	
	public int previous;

	public int getNext() {
		return next;
	}

	public void setNext(int next) {
		this.next = next;
	}

	public int getPrevious() {
		return previous;
	}

	public void setPrevious(int previous) {
		this.previous = previous;
	}

	public int getInputNumber() {
		return inputNumber;
	}

	public void setInputNumber(int number) {
		this.inputNumber = number;
	}

}
