package com.practice.controller;

public class CustomException extends RuntimeException {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public CustomException(String errorCode)
	{
		
		super(errorCode);
	}
 
}
