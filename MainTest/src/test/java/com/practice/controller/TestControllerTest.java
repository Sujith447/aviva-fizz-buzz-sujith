package com.practice.controller;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.Mock;

public class TestControllerTest {
	
	TestController testController;
	
	@Mock
	HttpServletRequest req;
	
	@Mock
	HttpServletResponse res;
	
	@Mock
	DayChecker dayChecker;
	
	
	@Test
	public void testCaseTest() throws IOException
	{
		RequestPojo pojo=new RequestPojo();
		pojo.setInputNumber(10);
		pojo.setNext(2);
		pojo.setPrevious(0);
		testController.testCase(pojo,req,res);
	}
	
	@Test
	public void requestBindingTest()
	{
		RequestPojo pojo=new RequestPojo();
		testController.requestBinding(10, 2, 0, pojo);
		assertEquals(10,pojo.getInputNumber());
		assertEquals(2,pojo.getNext());
		assertEquals(0,pojo.getPrevious());
	}

}
