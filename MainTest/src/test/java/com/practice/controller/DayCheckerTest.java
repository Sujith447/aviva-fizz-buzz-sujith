package com.practice.controller;

import static org.junit.Assert.*;

import org.junit.Test;

public class DayCheckerTest {
	
	DayChecker dayChecker=new DayChecker();
	
	@Test
	public void dayCheck()
	{
		assertEquals("Saturday",dayChecker.dayCheck());
	}

}
